#[allow(unused_macros)]
macro_rules! assert_max {
    ($arr:expr, $t:expr) => {
        assert_eq!(find_max_subarray(&$arr, 0, $arr.len() - 1), $t);
        assert_eq!(
            find_max_subarray_q(&$arr, 0, $arr.len() - 1),
            $t,
            "Q: {:?}",
            $arr
        );
        assert_eq!(
            find_max_subarray_l(&$arr, 0, $arr.len() - 1),
            $t,
            "L: {:?}",
            $arr
        );
    };
}

#[allow(unused_macros)]
macro_rules!  max_table {
    ($($name:ident, $arr:expr,$t:expr),*) => {
        $(#[test]
        fn $name(){
            assert_max!($arr,$t);
        })*
    };
}

#[allow(unused_macros)]
macro_rules!  cross_table {
    ($($name:ident, $arr:expr,$t:expr),*) => {
       $(#[test]
       fn $name(){
            assert_eq!(find_max_cross(
                &$arr, 0, $arr.len()/2, $arr.len() - 1), $t,
                "{:?}", $arr);
       })*
    };
}

cross_table![
    cross_just_increasing,
    [1, 2, 3, 4, 5, 6, 7],
    (0, 6, 28),
    cross_with_negatives,
    [-1, -2, 3, 4, 5, 6, -7],
    (2, 5, 18),
    cross_all_negatives,
    [-1, -2, -3, -4, -5, -6, -7],
    (3, 4, -9)
];

max_table![
    max_in_the_middle,
    [1, 2, -30, -1, 2, 5, 7, 10, -8],
    (4, 7, 24),
    max_just_increasing,
    [1, 2, 3, 4, 5, 6, 7],
    (0, 6, 28),
    max_with_negatives,
    [-1, -2, 3, 4, 5, 6, -7],
    (2, 5, 18),
    max_right,
    [-1, -2, -7, -4, -1, 3, 4, 5, 7, -2],
    (5, 8, 19),
    max_left,
    [-1, 3, 4, 5, 7, -2, -1, -2, -7, -4, -1, 2, 3, 4],
    (1, 4, 19),
    max_again_right,
    [-1, 3, 4, 5, 7, -2, -10, -100, -2, -7, -4, -1, 20, 3, 4, 10, -1,],
    (12, 15, 37),
    max_all_negatives,
    [-1, -1, -2, -4, -5, -6, -7],
    (0, 0, -1),
    max_more_negatives,
    [-7, -8, -4, -3, -2, -4, -5, -6, -7],
    (4, 4, -2)
];

#[test]
fn max_one() {
    let arr = [-1, -3, -4, -5, 7, -2, -10];
    assert_max!(arr, (4, 4, 7));
}

#[allow(unused_macros)]
macro_rules! log {
    ($x:expr) => {
        println!("{}", $x);
    };
}

#[allow(dead_code)]
fn find_max_subarray_l(arr: &[i32], low: usize, high: usize) -> (usize, usize, i32) {
    let mut sum = arr[0];
    let mut sum_p = arr[0];

    let mut l = 0;
    let mut h = 0;

    for i in low + 1..=high {
        let sum_n = sum + arr[i];
        let x = arr[i];

        sum = std::cmp::max(x, sum_n);

        if x > sum_n {
            l = i;
        }

        if sum > sum_p {
            sum_p = sum;
            h = i;
        }
    }

    if l > h {
        l = h;
    }
    (l, h, sum_p)
}

#[allow(dead_code)]
fn find_max_subarray(arr: &[i32], low: usize, high: usize) -> (usize, usize, i32) {
    let mut sum_p = arr[0];

    let mut l: usize = 0;
    let mut h: usize = 0;

    let mut compare = |sum, i, j| {
        if sum > sum_p {
            l = i;
            h = j;
            sum_p = sum;
        }
    };

    for i in low..high {
        let mut sum = arr[i];
        compare(sum, i, i);

        for j in i + 1..=high {
            sum += arr[j];
            compare(sum, i, j);
        }
    }

    (l, h, sum_p)
}

#[allow(dead_code)]
fn find_max_subarray_q(arr: &[i32], low: usize, high: usize) -> (usize, usize, i32) {
    if low == high {
        return (low, high, arr[low]);
    }

    let mid = (low + high) / 2;

    let (l_l, h_l, sum_l) = find_max_subarray_q(arr, low, mid);
    let (l_r, h_r, sum_r) = find_max_subarray_q(arr, mid + 1, high);
    let (l_c, h_c, sum_c) = find_max_cross(arr, low, mid, high);

    if sum_l >= sum_r && sum_l >= sum_c {
        return (l_l, h_l, sum_l);
    } else if sum_r >= sum_l && sum_r >= sum_c {
        return (l_r, h_r, sum_r);
    }

    (l_c, h_c, sum_c)
}

fn find_max_cross(arr: &[i32], low: usize, mid: usize, high: usize) -> (usize, usize, i32) {
    let get_sum = |r: Box<Iterator<Item = usize>>| {
        let mut s_p = std::i32::MIN;
        let mut s = 0;
        let mut idx = 0;

        for i in r {
            s += arr[i];
            if s > s_p {
                s_p = s;
                idx = i;
            }
        }
        (idx, s_p)
    };

    let (idx_l, sum_l) = get_sum(Box::new((low..=mid).rev()));
    let (idx_r, sum_r) = get_sum(Box::new(mid + 1..=high));

    return (idx_l, idx_r, sum_l + sum_r);
}
